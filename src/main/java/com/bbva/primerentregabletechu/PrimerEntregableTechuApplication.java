package com.bbva.primerentregabletechu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimerEntregableTechuApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrimerEntregableTechuApplication.class, args);
    }

}
