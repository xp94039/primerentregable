package com.bbva.primerentregabletechu.Servicio;

import com.bbva.primerentregabletechu.Modelo.Citas;
import com.bbva.primerentregabletechu.Modelo.Medico;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ServicioCitas {

    private List<Citas> dataList = new ArrayList<Citas>();
    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Citas> Citas = new ConcurrentHashMap<Long,Citas>();

    // READ Collections
    public List<Citas> getCitas() {
        return dataList;
    }

    // READ instance
    public Citas getCitas(long index) throws IndexOutOfBoundsException {
        if(getIndex(index)>=0) {
            return dataList.get(getIndex(index));
        }
        return null;
    }

    // Get index
    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<dataList.size()) {
            if(dataList.get(i).getIdCita() == index){
                return(i); }
            i++;
        }
        return -1;
    }
    // DELETE
    public void removeCitas(int index) throws IndexOutOfBoundsException {
        int pos = dataList.indexOf(dataList.get(index));
        dataList.remove(pos);
    }
    public Citas addCita(Citas newPa) {
        dataList.add(newPa);
        return newPa;
    }

    public void reemplazarCitas(long id, Citas p) {
        // FIXME: verificar que exista el producto con este id,
        // antes de reemplazar.
        p.idCita = id;
        this.Citas.put(id, p);
    }

}
