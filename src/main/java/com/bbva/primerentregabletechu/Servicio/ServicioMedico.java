package com.bbva.primerentregabletechu.Servicio;

import com.bbva.primerentregabletechu.Modelo.Medico;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ServicioMedico {

    private List<Medico> dataList = new ArrayList<Medico>();

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Medico> Medico = new ConcurrentHashMap<Long,Medico>();

    /*public ServicioMedico() {
        dataList.add(new Medico(1, "Jahn", "bedon","Masculino","71484685","60",1.65,28, new Medico(1,"thais","bardales","Nutricionista")));
    }*/
    // READ ALL
    public List<Medico> getMedicos() {
        return dataList;
    }

    // READ FOR ID
    public Medico getMedico(long index) throws IndexOutOfBoundsException {
        if(getIndex(index)>=0) {
            return dataList.get(getIndex(index));
        }
        return null;
    }

    // CREATE
    public Medico addMedico(Medico newPa) {
        dataList.add(newPa);
        return newPa;
    }

    // UPDATE
    public Medico updateMedico(int index, Medico newPa)
            throws IndexOutOfBoundsException {
        dataList.set(index, newPa);
        return dataList.get(index);
    }

    // DELETE
    public void removeMedico(int index) throws IndexOutOfBoundsException {
        int pos = dataList.indexOf(dataList.get(index));
        dataList.remove(pos);
    }

    // Get index
    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<dataList.size()) {
            if(dataList.get(i).getIdMedico() == index){
                return(i); }
            i++;
        }
        return -1;
    }

    public long agregar(Medico p) {
        p.idMedico = this.secuenciador.incrementAndGet();
        this.Medico.put(p.idMedico, p);
        return p.idMedico;
    }

    public void reemplazarMedico(long id, Medico p) {
        // FIXME: verificar que exista el producto con este id,
        // antes de reemplazar.
        p.idMedico = id;
        this.Medico.put(id, p);
    }


}
