package com.bbva.primerentregabletechu.Servicio;

import com.bbva.primerentregabletechu.Modelo.Medico;
import com.bbva.primerentregabletechu.Modelo.Paciente;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ServicioPaciente {
    private List<Paciente> dataList = new ArrayList<Paciente>();

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Paciente> paciente = new ConcurrentHashMap<Long,Paciente>();

    /*public ServicioPaciente() {
        dataList.add(new Paciente(1, "Jahn", "bedon","Masculino","71484685","60",1.65,28, new Medico(1,"thais","bardales","Nutricionista")));
    }*/
    // READ ALL
    public List<Paciente> getPacientes() {
        return dataList;
    }

    // READ FOR ID
    public Paciente getPaciente(long index) throws IndexOutOfBoundsException {
        if(getIndex(index)>=0) {
            return dataList.get(getIndex(index));
        }
        return null;
    }

    // CREATE
    public Paciente addPaciente(Paciente newPa) {
        dataList.add(newPa);
        return newPa;
    }

    // UPDATE
    public Paciente updatePaciente(int index, Paciente newPa)
            throws IndexOutOfBoundsException {
        dataList.set(index, newPa);
        return dataList.get(index);
    }

    // DELETE
    public void removePaciente(int index) throws IndexOutOfBoundsException {
        int pos = dataList.indexOf(dataList.get(index));
        dataList.remove(pos);
    }

    // Get index
    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<dataList.size()) {
            if(dataList.get(i).getIdPaciente() == index){
                return(i); }
            i++;
        }
        return -1;
    }

    public long agregar(Paciente p) {
        p.idPaciente = this.secuenciador.incrementAndGet();
        this.paciente.put(p.idPaciente, p);
        return p.idPaciente;
    }

    public void reemplazarPaciente(long id, Paciente p) {
        // FIXME: verificar que exista el producto con este id,
        // antes de reemplazar.
        p.idPaciente = id;
        this.paciente.put(id, p);
    }

}
