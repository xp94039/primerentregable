package com.bbva.primerentregabletechu.Modelo;

import java.util.ArrayList;
import java.util.List;

public class Citas {

    public long idCita;
    public long idPaciente;
    private long idMedico;
    private String fechaCita;
    private List<Medico> medico;

    public Citas() {

    }

    public Citas(long idCita, long idPaciente,long idMedico, String fechaCita,List<Medico> medico) {
        this.idCita = idCita;
        this.idPaciente = idPaciente;
        this.idMedico = idMedico;
        this.fechaCita = fechaCita;
        this.medico =medico;

    }

    public long getIdCita() {
        return idCita;
    }

    public void setIdCita(long idCita) {
        this.idCita = idCita;
    }

    public long getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(long idMedico) {
        this.idMedico = idMedico;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public List<Medico> getMedico() {
        if (this.medico == null) {
            this.medico = new ArrayList<>();
        }
        return medico;
    }

    public void setMedico(List<Medico> medico) {
        this.medico = medico;
    }

    public long getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(long idPaciente) {
        this.idPaciente = idPaciente;
    }
}
