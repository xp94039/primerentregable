package com.bbva.primerentregabletechu.Modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Paciente  {
    public  long idPaciente;
    private String nombre;
    private String apellidos;
    private String genero;
    private String dni;
    private String peso;
    private double altura;
    private int edad;
    private List<Pago> formaDePago;
    private List<Citas> citas;


    public Paciente(long idPaciente, String nombre, String apellidos, String genero, String dni, String peso, double altura, int edad, List<Pago> formaDePago, List<Citas> citas) {
        this.idPaciente = idPaciente;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.genero = genero;
        this.dni = dni;
        this.peso = peso;
        this.altura = altura;
        this.edad = edad;
        this.formaDePago = formaDePago;
        this.citas = citas;
    }

    public long getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public List<Pago> getFormaDePago() {
        if (this.formaDePago == null) {
            this.formaDePago = new ArrayList<>();
        }
        return formaDePago;
    }

    public void setFormaDePago(List<Pago> formaDePago) {
        this.formaDePago = formaDePago;
    }

    public List<Citas> getCitas() {

        if(this.citas == null){
            this.citas = new ArrayList<>();
        }

        return citas;
    }

    public void setCitas(List<Citas> citas) {
        this.citas = citas;
    }
}
