package com.bbva.primerentregabletechu.Controlador;

import com.bbva.primerentregabletechu.Modelo.Citas;
import com.bbva.primerentregabletechu.Modelo.Medico;
import com.bbva.primerentregabletechu.Modelo.Paciente;
import com.bbva.primerentregabletechu.Servicio.ServicioCitas;
import com.bbva.primerentregabletechu.Servicio.ServicioMedico;
import com.bbva.primerentregabletechu.Servicio.ServicioPaciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("${api.v1}")
public class ControladorCitas {


    private final AtomicLong secuenciaIds = new AtomicLong(0L);

    @Autowired
    ServicioPaciente servicioPaciente;
    @Autowired
    ServicioCitas servicioCitas;

    @Autowired
    ServicioMedico servicioMedico;


    // Get de todos las citas
    @GetMapping("/citas")
    public  List<Citas> getCitas() {
        return servicioCitas.getCitas();
    }

   // Get de citas por id del paciente
    @GetMapping("/pacientes/{idPaciente}/citas")
    public ResponseEntity citasPorPaciente(@PathVariable long idPaciente) {
        Paciente pr = servicioPaciente.getPaciente(idPaciente);
        if (pr == null) {
            return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);

    }
    // POST para agregar citas del paciente
    @PostMapping("/citas")
    public ResponseEntity<String> agregarCitas(@RequestBody Citas cita) {
        Medico m = servicioMedico.getMedico(cita.getIdMedico());
        Paciente p = servicioPaciente.getPaciente(cita.getIdPaciente());
        if(p == null && m == null){
            return new ResponseEntity<>("Paciente y Medico no registrado.", HttpStatus.NOT_FOUND);
        }else if(m == null){
            return new ResponseEntity<>("Medico no registrado.", HttpStatus.NOT_FOUND);
        } else if(p == null){
            return new ResponseEntity<>("Paciente no registrado.", HttpStatus.NOT_FOUND);
        } else {
            long idCitaMe= this.secuenciaIds.incrementAndGet();
            cita.setIdCita(idCitaMe);
            p.getCitas().add(cita);
            cita.getMedico().add(m);
            servicioCitas.addCita(cita);

            return new ResponseEntity<>("Cita creada satisfactoriamente!", HttpStatus.CREATED);
        }
    }

    // PATCH para agregar citas de pacientes por ID
    @PatchMapping("/pacientes/{idPaciente}/citas")
    public ResponseEntity agregarCitasPaciente(@PathVariable long idPaciente, @RequestBody Citas cit) {
        Paciente pa = servicioPaciente.getPaciente(idPaciente);
        Medico me = servicioMedico.getMedico(cit.getIdMedico());
        if (pa != null && me != null) {
                long idCita= this.secuenciaIds.incrementAndGet();
                cit.setIdCita(idCita);
                pa.getCitas().add(cit);
                cit.getMedico().add(me);
                servicioCitas.addCita(cit);

        } else {
            return new ResponseEntity<>("Paciente o Medico no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(cit);
    }

    // DELETE citas
    @DeleteMapping("/citas/{id}")
    public ResponseEntity deleteCitas(@PathVariable Integer id) {
        Citas pr = servicioCitas.getCitas(id);
        if(pr == null) {
            return new ResponseEntity<>("Cita no encontrada.", HttpStatus.NOT_FOUND);
        }
        servicioCitas.removeCitas(id - 1);
        return new ResponseEntity<>("Cita eliminado correctamente.", HttpStatus.OK); // También 204 No Content
    }

    // PUT modificar citas por Id de cita
    @PutMapping("/citas/{idCitas}")
    public ResponseEntity updateCitas(@PathVariable(name = "idCitas") long id,
                                       @RequestBody Citas p) {
        try {
            Citas pr = servicioCitas.getCitas(id);
            Medico me = servicioMedico.getMedico(p.getIdMedico());
            Paciente pa = servicioPaciente.getPaciente(p.getIdPaciente());

           if(pr == null){
                return new ResponseEntity<>("Cita no encontrada.", HttpStatus.NOT_FOUND);
            } else if(me == null){
                return new ResponseEntity<>("Medico no encontrado.", HttpStatus.NOT_FOUND);
            } else if(pa == null ){
               return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
           } else {
               pr.setIdPaciente(p.getIdPaciente());
               pr.setIdMedico(p.getIdMedico());
                //pr.getMedico().add(me);
               pr.setFechaCita(p.getFechaCita());
               this.servicioCitas.reemplazarCitas(id, pr);
               return new ResponseEntity<>("Cita actualizada correctamente.", HttpStatus.OK);
            }

        }catch (Exception x){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }
}
