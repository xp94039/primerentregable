package com.bbva.primerentregabletechu.Controlador;

import com.bbva.primerentregabletechu.Modelo.Citas;
import com.bbva.primerentregabletechu.Servicio.ServicioCitas;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbva.primerentregabletechu.Modelo.Medico;
import com.bbva.primerentregabletechu.Servicio.ServicioMedico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("${api.v1}")
public class ControladorMedico {

    @Autowired
    ServicioMedico servicioMedico;
    @Autowired
    ServicioCitas servicioCitas;

    @GetMapping("")
    public String root() {
        return "Techu API REST v1.0.0";
    }

    // Get de todos los Pacientes
    @GetMapping("/medicos")
    public  List<Medico> getMedicos() {
        return servicioMedico.getMedicos();
    }

    // GET tamaño
    @GetMapping("/medicos/size")
    public int countmedicos() {
        return servicioMedico.getMedicos().size();
    }

    // GET instancia por id
    @GetMapping("/medicos/{id}")
    public ResponseEntity getMedicoId(@PathVariable int id) {
        Medico pr = servicioMedico.getMedico(id);
        if (pr == null) {
            return new ResponseEntity<>("medico no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    // POST
    @PostMapping("/medicos")
    public ResponseEntity<String> addMedico(@RequestBody Medico medico) {
        this.servicioMedico.agregar(medico);
        servicioMedico.addMedico(medico);
        return new ResponseEntity<>("medico created successfully!", HttpStatus.CREATED);
    }

    // PUT
    @PutMapping("/medicos/{idMedico}")
    public ResponseEntity updateMedico(@PathVariable(name = "idMedico") long id,
                                         @RequestBody Medico p) {
      try {
          Medico pr = servicioMedico.getMedico(id);
          if (pr == null) {
              return new ResponseEntity<>("Medico no encontrado.", HttpStatus.NOT_FOUND);
          }
          pr.setNombre(p.getNombre());
          pr.setApellidos(p.getApellidos());
          pr.setEspecialidad(p.getEspecialidad());
          pr.setPrecioConsulta(p.getPrecioConsulta());
          this.servicioMedico.reemplazarMedico(id, pr);

          return new ResponseEntity<>("Medico actualizado correctamente.", HttpStatus.OK);

      }catch (Exception x){
          throw new ResponseStatusException(HttpStatus.FORBIDDEN);
      }
    }

    // DELETE
    @DeleteMapping("/medicos/{id}")
    public ResponseEntity deleteMedico(@PathVariable Integer id) {
        Medico pr = servicioMedico.getMedico(id);
        Citas cita= servicioCitas.getCitas(id);
        if(pr == null) {
            return new ResponseEntity<>("Medico no encontrado.", HttpStatus.NOT_FOUND);
        }else if( cita != null && cita.getIdMedico() == id){
            return new ResponseEntity<>("Medico ya tiene asignado una cita, primero elimine cita.", HttpStatus.CONFLICT);
        } else {
            servicioMedico.removeMedico(id - 1);
            return new ResponseEntity<>("Medico eliminado correctamente.", HttpStatus.OK); // También 204 No Content

        }

    }

}
