package com.bbva.primerentregabletechu.Controlador;

import com.bbva.primerentregabletechu.Modelo.Citas;
import com.bbva.primerentregabletechu.Modelo.Medico;
import com.bbva.primerentregabletechu.Modelo.Paciente;
import com.bbva.primerentregabletechu.Servicio.ServicioCitas;
import com.bbva.primerentregabletechu.Servicio.ServicioMedico;
import com.bbva.primerentregabletechu.Servicio.ServicioPaciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.v1}")
public class ControladorPaciente {
    @Autowired
     ServicioPaciente servicioPaciente;
    @Autowired
    ServicioCitas servicioCitas;

    // Get de todos los Pacientes
    @GetMapping("/pacientes")
    public  List<Paciente> getPacientes() {
        return servicioPaciente.getPacientes();
    }

    // GET tamaño
    @GetMapping("/pacientes/size")
    public int countpacientes() {
        return servicioPaciente.getPacientes().size();
    }

    // GET instancia por id
    @GetMapping("/pacientes/{id}")
    public ResponseEntity getPacienteId(@PathVariable int id) {
        Paciente pr = servicioPaciente.getPaciente(id);
        if (pr == null) {
            return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    // POST
    @PostMapping("/pacientes")
    public ResponseEntity<String> addPaciente(@RequestBody Paciente paciente) {
        this.servicioPaciente.agregar(paciente);
        servicioPaciente.addPaciente(paciente);
        return new ResponseEntity<>("Paciente creado Satisfactoriamente!", HttpStatus.CREATED);
    }

    // PUT
    @PutMapping("/pacientes/{idPaciente}")
    public ResponseEntity updatePaciente(@PathVariable(name = "idPaciente") long id,
                                         @RequestBody Paciente p) {
        Paciente pr = servicioPaciente.getPaciente(id);
        if (pr == null) {
            return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setNombre(p.getNombre());
        pr.setApellidos(p.getApellidos());
        pr.setEdad(p.getEdad());
        pr.setAltura(p.getAltura());
        pr.setDni(p.getDni());
        pr.setGenero(p.getGenero());
        pr.setPeso(p.getPeso());
        pr.setFormaDePago(p.getFormaDePago());

        this.servicioPaciente.reemplazarPaciente(id, pr);

        return new ResponseEntity<>("Paciente actualizado correctamente.", HttpStatus.OK);
    }

    // DELETE
    @DeleteMapping("/pacientes/{id}")
    public ResponseEntity deletePaciente(@PathVariable Integer id) {
        Paciente pr = servicioPaciente.getPaciente(id);
        Citas cita= servicioCitas.getCitas(id);

        if(pr == null) {
            return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
        }else if( cita != null && cita.getIdPaciente() == id){
            return new ResponseEntity<>("Paciente ya tiene registrado una cita, primero elimine cita.", HttpStatus.CONFLICT);
        } else{
            servicioPaciente.removePaciente(id - 1);
            return new ResponseEntity<>("Paciente eliminado correctamente.", HttpStatus.OK); // También 204 No Content

        }

    }



}
